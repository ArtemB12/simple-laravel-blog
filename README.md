## Summary

This is a simple Laravel 10 project where you can register a user and sign in; create, update and delete a post.
<br>
Styles managed with Bootstrap library.

## Launch

- check your server running (XAMPP etc.)
- run 'npm run dev' in a terminal
- run 'php artisan serve' in another terminal window
- see the project at you localhost URL

